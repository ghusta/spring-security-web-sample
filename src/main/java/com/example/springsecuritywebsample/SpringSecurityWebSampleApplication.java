package com.example.springsecuritywebsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityWebSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityWebSampleApplication.class, args);
	}

}
