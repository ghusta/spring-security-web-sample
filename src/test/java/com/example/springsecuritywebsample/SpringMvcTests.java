package com.example.springsecuritywebsample;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@SpringBootTest
@WebMvcTest
class SpringMvcTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/home").with(anonymous()))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Welcome!")))
                .andExpect(content().string(containsString("Logged user: <span>anonymous</span>")));
    }

    @Test
    @WithMockUser(username = "toto")
    void shouldReturnCustomMessageWhenLogged() throws Exception {
        this.mockMvc.perform(get("/home"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Welcome!")))
                .andExpect(content().string(containsString("Logged user: <span>toto</span>")));
    }

    @Test
    void shouldReturnCustomMessageWhenBobLogged() throws Exception {
        this.mockMvc.perform(get("/home").with(bob()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Welcome!")))
                .andExpect(content().string(containsString("Logged user: <span>bob</span>")));
    }

    public static RequestPostProcessor bob() {
        return user("bob").password("bob").roles("USER", "BOSS");
    }

    @Test
    @WithMockUser(username = "toto", roles = {"ADMIN"})
    void shouldAdminPageBeSecured() throws Exception {
        this.mockMvc.perform(get("/admin/secured"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("This page is reserved")));
    }

    @Test
    void shouldAdminPageBeRedirectedIfUnauthenticated() throws Exception {
        this.mockMvc.perform(get("/admin/secured"))
                .andDo(print())
                .andExpect(unauthenticated())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));
    }
}
